import { Component } from '@angular/core';
import { Tarea } from '../models/tarea.model';

@Component({
  selector: 'app-lista-tareas',
  templateUrl: './lista-tareas.component.html',
  styleUrls: ['./lista-tareas.component.css']
})
export class ListaTareasComponent {

  tareas: Tarea[];

  constructor() {
    this.tareas = [];
  }

  guardarTarea(nombre: string, descripcion: string) {
    this.tareas.push( new Tarea( nombre, descripcion ) );
    return false;
  }
}
