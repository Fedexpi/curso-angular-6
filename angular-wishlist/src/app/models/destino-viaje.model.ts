export class DestinoViaje {
    nombre: string;
    imagenUrl: string;

    constructor( nombre, imagenUrl ) {
        this.nombre = nombre;
        this.imagenUrl = imagenUrl;
    }
}