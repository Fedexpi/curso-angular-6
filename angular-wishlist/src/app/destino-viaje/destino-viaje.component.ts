import { Component, OnInit, Input, HostBinding } from '@angular/core';

@Component({
  selector: 'app-destino-viaje',
  templateUrl: './destino-viaje.component.html',
  styleUrls: ['./destino-viaje.component.css']
})
export class DestinoViajeComponent implements OnInit {
  @Input() destino: string;
  @HostBinding('attr.class') cssClass = 'col-md-4';
  constructor() { }

  ngOnInit() {
  }

}
